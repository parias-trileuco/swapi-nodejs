/**
 * swapiMiddlewareResolver: long description for the file
 *
 * @summary swapiMiddlewareResolver for onboarding in Trileuco Solutions.
 * @author Pedro Arias Ruiz <parias@trileucosolutions.com>
 *
 * Created at     : 07/30/2020
 * Last modified  : 08/07/2020
 * Sources:
 * https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
 * https://stackoverflow.com/questions/22949597/getting-max-values-in-json-array
 */

/**
 * swapiMiddlewareResolver Imports
 */
const { pick } = require('lodash');
const {fetchJsonDataFromSwapi, queryBuilder} = require('./utils/swapiFetcher');
const {getMax} = require('./utils/swapiUtils')
const _ = require('lodash');
const _films = '/films';
const _people = '/people';
const _vehicles = '/vehicles';
const _starships = '/starships';
const _species = '/species';
const _planets = '/planets';

/**
 * SwapiMiddlewareResolver API Exports
 */

module.exports = {
    /**
     * @abstract getFilmsData: Get Paged or Indexed Data from Films.
     * @param {Integer} index 
     * @param {Boolean} isPage
     * @returns {JSON}
     */
    getFilmsData: getFilmsData = (index, isPage) => {
        return fetchJsonDataFromSwapi(queryBuilder(_films, index, isPage));
    },

    /**
     * @abstract getPeopleData: Get Paged or Indexed Data from People.
     * @param {Integer} index 
     * @param {Boolean} isPage
     * @returns {JSON}
     */
    getPeopleData: getPeopleData = (index, isPage) => {
        return fetchJsonDataFromSwapi(queryBuilder(_people, index, isPage));
    },

    /**
     * @abstract getVehiclesData: Get Paged or Indexed Data from Vehicle.
     * @param {Integer} index 
     * @param {Boolean} isPage
     * @returns {JSON}
     */
    getVehiclesData: getVehiclesData = (index, isPage) => {
        return fetchJsonDataFromSwapi(queryBuilder(_vehicles, index, isPage));
    },

    /**
     * @abstract getStarshipsData: Get Paged or Indexed Data from Starships.
     * @param {Integer} index 
     * @param {Boolean} isPage 
     * @returns {JSON}
     */
    getStarshipsData: getStarshipsData = (index, isPage) => {
        return fetchJsonDataFromSwapi(queryBuilder(_starships, index, isPage));
    },

    /**
     * @abstract getSpeciesData: Get Paged or Indexed Data from Species.
     * @param {Integer} input 
     * @param {Boolean} isPage 
     * @returns {JSON}
     */
    getSpeciesData: getSpeciesData = (index, isPage) => {
        return fetchJsonDataFromSwapi(queryBuilder(_species, index, isPage));
    },

    /**
     * @abstract getPlanetsData: Get Paged or Indexed Data from Planets.
     * @param {Integer} index 
     * @param {Boolean} isPage
     * @returns {JSON}
     */
    getPlanetsData: getPlanetsData = (index, isPage) => {
        return fetchJsonDataFromSwapi(queryBuilder(_planets, index, isPage));
    },

    /**
     * @abstract searchFilmsData: Search item within Films Data.
     * @param {String} input
     * @returns {JSON}
     */
    searchFilmsData: searchFilmsData = (input) => {
        return fetchJsonDataFromSwapi(queryBuilder(_films, input, false, true));
    },

    /**
     * @abstract searchPeopleData: Search item within People Data.
     * @param {String} input
     * @returns {JSON}
     */
    searchPeopleData: searchPeopleData = (input) => {
        return fetchJsonDataFromSwapi(queryBuilder(_people, input, false, true));
    },

    /**
     * @abstract searchVehilesData: Search item within Vehicles Data.
     * @param {String} input
     * @returns {JSON} 
     */
    searchVehiclesData: searchVehiclesData = (input) => {
        return fetchJsonDataFromSwapi(queryBuilder(_vehicles, input, false, true));
    },

    /**
     * @abstract searchStarshipsData: Search item within Starships Data.
     * @param {String} input
     * @returns {JSON} 
     */
    searchStarshipsData: searchStarshipsData = (input) => {
        return fetchJsonDataFromSwapi(queryBuilder(_starships, input, false, true));
    },
    
    /**
     * @abstract searchSpeciesData: Search item within Species Data.
     * @param {String} input
     * @returns {JSON} 
     */
    searchSpeciesData: searchSpeciesData = (input) => {
        return fetchJsonDataFromSwapi(queryBuilder(_species, input, false, true));
    },

    /**
     * @abstract searchPlanetsData: Search item within Planets Data.
     * @param {String} input
     * @returns {JSON} 
     */
    searchPlanetsData: searchPlanetsData = (input) => {
        return fetchJsonDataFromSwapi(queryBuilder(_planets, input, false, true));
    },

    /**
     * @abstract searchPlanetsData: Search item within Planets Data.
     * @param {JSON} input
     * @returns {JSON} 
     */
    mapPersonInfo: async function mapPersonInfo(json_response) {
        var _personalInfo = {};
        var _transportData = [];
        var _jsonPersonData = json_response.results[0];

        for (var key of Object.keys(_jsonPersonData)) {
            if(key === 'name') {
                _personalInfo['name'] = _jsonPersonData['name'];

            } else if(key === 'gender') {
                _personalInfo['gender'] = _jsonPersonData['gender'];

            } else if(key === 'homeworld') {
                var _planetsInfo = await fetchJsonDataFromSwapi(_jsonPersonData['homeworld'])
                _personalInfo['planet_name'] = _planetsInfo['name'];

            } else if(key === 'vehicles' && _jsonPersonData['vehicles'].length) {
                await Promise.all(_jsonPersonData['vehicles'].map(async (url) => {
                    var _vehiclesInfo = await fetchJsonDataFromSwapi(url);
                    _transportData.push({'name':_vehiclesInfo['name'], 'max_atmosphering_speed': _vehiclesInfo['max_atmosphering_speed']});
                    return {'name':_vehiclesInfo['name'], 'max_atmosphering_speed': _vehiclesInfo['max_atmosphering_speed']};
                }));
      
            } else if(key === 'starships' && _jsonPersonData['starships'].length) {
                await Promise.all(_jsonPersonData['starships'].map(async (url) => {
                    var _starshipsInfo = await fetchJsonDataFromSwapi(url);
                    _transportData.push({'name':_starshipsInfo['name'], 'max_atmosphering_speed': _starshipsInfo['max_atmosphering_speed']});
                    return {'name':_starshipsInfo['name'], 'max_atmosphering_speed': _starshipsInfo['max_atmosphering_speed']};
                }));

            } else if(key === 'films') {
                var _filmsData = await Promise.all(_jsonPersonData['films'].map(async (url) => {
                    var _filmsInfo = await fetchJsonDataFromSwapi(url);
                    return {'title':_filmsInfo['title'], 'release_date': _filmsInfo['release_date']};
                }));
                _personalInfo['films'] = _filmsData;
            }
        }
        
        const { get, maxBy } = require('lodash');
        const fastest = maxBy(_transportData, 'max_atmosphering_speed');
        _personalInfo['fastest_vehicle_driven'] = get(fastest, 'name', 'n/a');
        return _personalInfo;
    },
    /**
     * @abstract mapPersonInfoPage: Create a Person Info 
     * @param {JSON} input
     * @returns {JSON} 
     */
    mapPersonInfoPage: async function mapPersonInfoPage(json_response) {
        try {
            const _personalInfoPage = await Promise.all(json_response.results.map((_personalInfoItem) =>  {
                return module.exports.mapPersonInfo({"results": [_personalInfoItem]})
            }));

            return { ...pick(json_response, ['count', 'next', 'previous']), results: _personalInfoPage }

        } catch (e) {
            console.error(e)
        }
    }

}