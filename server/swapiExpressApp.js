/**
 * SwapiExpressApp: long description for the file
 *
 * @summary swapiExpressApp for onboarding in Trileuco Solutions.
 * @author Pedro Arias Ruiz <parias@trileucosolutions.com>
 *
 * Created at     : 07/30/2020
 * Last modified  : 21/09/2020
 */

/**
 * @abstract swapiExpressApp Imports
 */
const swapiMiddlewareResolver = require('./swapiMiddlewareResolver.js');
const express = require('express');
const cors = require('cors');
const path = require('path');

const manageResponse = (res, promise) => {
  promise
    .then(value => res.json(value))
    .catch(err => console.log(`There has been a problem with your api call: ${err.message}`));
}

/**
 * @abstract swapiExpressApp Const
 */
const app = express();
const port = 8080;

/**
 * @abstract swapiExpressApp Setup:
 * @notice Enable CORS
 */
app.use(cors());

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

/**
 * @abstract API_CALL: GET Async Request Films
 */
app.get('/swapi-proxy/films', (_, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getFilmsData());
});

/**
* @abstract API_CALL: GET Async Request Film By ID
*/
app.get('/swapi-proxy/films/:filmId', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getFilmsData(req.params.filmId));
});

/**
 * @abstract API_CALL: GET Async Request People
 */
app.get('/swapi-proxy/people', (_, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getPeopleData());
});

/**
 * @abstract API_CALL: GET Async Request People By ID
 */
app.get('/swapi-proxy/people/:peopleId', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getPeopleData(req.params.peopleId));
});

/**
 * @abstract API_CALL: GET Request Vehicles
 */
app.get('/swapi-proxy/vehicles', (_, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getVehiclesData());
});

/**
 * @abstract API_CALL: GET Request Vehicle By ID
 */
app.get('/swapi-proxy/vehicles/:vehicleId', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getVehiclesData(req.params.vehicleId));
});

/**
 * @abstract API_CALL: GET Request Starships
 */
app.get('/swapi-proxy/starship', (_, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getStarshipsData());
});

/**
 * @abstract API_CALL: GET Request Starship By ID
 */
app.get('/swapi-proxy/starship/:startshipId', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getStarshipsData(req.params.startshipId));
});

/**
 * @abstract API_CALL: GET Request Species
 */
app.get('/swapi-proxy/species', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getSpeciesData()); 
});

/**
 * @abstract API_CALL: GET Request Species By ID
 */
app.get('/swapi-proxy/species/:specieId', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getSpeciesData(req.params.specieId)); 
});

/**
 * @abstract API_CALL: GET Request Planets
 */
app.get('/swapi-proxy/planets', (_, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getPlanetsData());
});

/**
* @abstract API_CALL: GET Request Planet By ID
*/
app.get('/swapi-proxy/planets/:planetId', (req, res) => {
  return manageResponse(res, swapiMiddlewareResolver.getPlanetsData(req.params.planetId));
});

/**
* @abstract API_CALL: GET Request Person Information By Name or PageId
*/
app.get('/swapi-proxy/person-info', async (req, res) => {
  try {
    if(req.query.name !== undefined) {
      let _personData = await swapiMiddlewareResolver.searchPeopleData(req.query.name);
      let _personInfo = await swapiMiddlewareResolver.mapPersonInfo(_personData);
      return res.json(_personInfo);
    } 

    let _personData = await swapiMiddlewareResolver.getPeopleData(req.query.page, true);
    let _personInfoPage = await swapiMiddlewareResolver.mapPersonInfoPage(_personData);
    return res.json(_personInfoPage);
    
  } catch(error) {
    return res.sendStatus(404);
  }
});

app.listen(port, () => console.log(`[ swapiExpressApp ]: Listening on Port ${port}!`))
