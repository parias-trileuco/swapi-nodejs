const fetch = require('node-fetch');

const _swapiUri = 'http://swapi.trileuco.com/api';
const _pageQuery = '/?page=';
const _searchQuery = '/?search=';

module.exports = {
    fetchJsonDataFromSwapi: 
    /**
     * @abstract fetchJsonDataFromSwapi: 
     * @param {String} resourcePath 
     * @param {Integer} timeout 
     * @returns {JSON}
     */
    async function fetchJsonDataFromSwapi(resourcePath) {
        await new Promise(r => setTimeout(r, 500));
        console.log(` [ swapiMiddlewareResolver ]: Fetching Json Data From: ${resourcePath}`)
        var response = await fetch(resourcePath);
        var resourceData = await response.json();

        if (!response.ok) {
            throw new Error(` [ swapiMiddlewareResolver ]: Fetching Json Data HTTP Status: ${response.status}`);
        }
        return resourceData;
    },
    queryBuilder: 
    /**
     * @abstract queryBuilder: Query Builder for the swapiMiddlewareResolver adding (/?search= or /?page=' or /[index])
     * to the default domain.
     * @param {String} resourcePath 
     * @param {String} input 
     * @param {Boolean} isPage 
     * @param {Boolean} isQuery 
     * @returns {String}
     */
    function queryBuilder(resourcePath, input, isPage = false, isQuery = false) {
        let encodedInput = encodeURI(input);
        const baseResourcePath = _swapiUri + resourcePath;
        if (input !== undefined) {
            if (isPage === true) {
                console.log(` [ swapiMiddlewareResolver ]: Fetching Paged Resource ...`);
                return `${baseResourcePath}${_pageQuery}${encodedInput}`; 
            }
            if (isQuery === true) {
                console.log(` [ swapiMiddlewareResolver ]: Fetching Searched Resource ...`);
                return `${baseResourcePath}${_searchQuery}${encodedInput}`;
            }
            console.log(` [ swapiMiddlewareResolver ]: Fetching Indexed Resource ...`);
            return `${baseResourcePath}/${encodedInput}/`;
        }
        console.log(` [ swapiMiddlewareResolver ]: Fetching Group Resource ...`);
        return baseResourcePath;
    }
}