module.exports = {
    /**
    * @abstract getMax: Get Max entrie value within a JSON Object
    * @param {Array} arr 
    * @param {String} prop 
    */
    getMax: getMax = (arr, prop) =>  {
     var max;
     for (var i = 0; i < arr.length; i++) {
       if (max == null || parseInt(arr[i][prop]) > parseInt(max[prop]))
         max = arr[i];
     }
     return max;
   }
}