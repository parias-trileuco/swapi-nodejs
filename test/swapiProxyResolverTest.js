
const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticPersonInfo, staticPersonInfoPage} = require('./expectedResults.js');


describe('Swapi Proxy API', () =>  {

  it('It should return a JSON Object with mapPersonInfo data', async () => {
    let peopleJsonData = await swapiMiddlewareResolver.searchPeopleData('Luke Skywalker');
    let mapPersonInfoJsonData = await swapiMiddlewareResolver.mapPersonInfoPage(peopleJsonData);
    assert.strictEqual(deepCompare(staticPersonInfo, mapPersonInfoJsonData.results[0]), true);
  }).timeout(30000);

  it('It should return a JSON Object List with mapPersonInfoPage data', async () => {
    let peopleJsonData = await swapiMiddlewareResolver.getPeopleData(2, true);
    let mapPersonInfoPageJsonData = await swapiMiddlewareResolver.mapPersonInfoPage(peopleJsonData);
    assert.strictEqual(deepCompare(staticPersonInfoPage, mapPersonInfoPageJsonData), true); 
  }).timeout(30000);
});
