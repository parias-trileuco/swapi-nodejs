const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticVehicles} = require('./expectedResults');

describe('Swapi Vehicles API', () => {
  it('It should return a JSON Object with getVehiclesData data', async () =>  {
    let vehicleJsonData = await swapiMiddlewareResolver.getVehiclesData();
    assert.strictEqual(deepCompare(staticVehicles, vehicleJsonData.results[0]), true);
  }).timeout(5000);
  
  it('It should return a unique JSON Object with getVehiclesData data', async () =>  {
    let vehicleJsonData = await swapiMiddlewareResolver.getVehiclesData(4);
    assert.strictEqual(deepCompare(staticVehicles, vehicleJsonData), true);
  }).timeout(5000);

  it('It should return a JSON Object with searchVehiclesData data', async () => {
    let vehicleJsonData = await swapiMiddlewareResolver.searchVehiclesData('Sand Crawler');
    assert.strictEqual(deepCompare(staticVehicles, vehicleJsonData.results[0]), true);
  }).timeout(5000);
});