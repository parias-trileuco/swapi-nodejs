const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticStarship} = require('./expectedResults');

describe('Swapi Starships API', () => {
  it('It should return a JSON Object with getStarshipsData data', async () => {
    let starshipsJsonData = await swapiMiddlewareResolver.getStarshipsData();
    assert.strictEqual(deepCompare(staticStarship, starshipsJsonData.results[0]), true);
  }).timeout(5000);
  
  it('It should return a unique JSON Object with getStarshipsData data', async () => {
    let starshipsJsonData = await swapiMiddlewareResolver.getStarshipsData(2);
    assert.strictEqual(deepCompare(staticStarship, starshipsJsonData), true);
  }).timeout(5000);

  it('It should return a JSON Object with searchStarshipData data', async () => {
    let starshipsJsonData = await swapiMiddlewareResolver.searchStarshipsData('CR90 corvette');
    assert.strictEqual(deepCompare(staticStarship, starshipsJsonData.results[0]), true);
  }).timeout(5000);
});
