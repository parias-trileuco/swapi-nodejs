const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticPeople} = require('./expectedResults');

describe('Swapi People API', () => {
    it('It should return a JSON Object with getPeopleData data', async () => {
        let peopleJsonData = await swapiMiddlewareResolver.getPeopleData();
        assert.strictEqual(deepCompare(staticPeople, peopleJsonData.results[0]), true);  
    }).timeout(5000);
      
    it('It should return a unique JSON Object with getPeopleData data', async () => {
      let peopleJsonData = await swapiMiddlewareResolver.getPeopleData(1);
      assert.strictEqual(deepCompare(staticPeople, peopleJsonData), true);
    }).timeout(5000);

    it('It should return a JSON Object with searchPeopleData data', async () => { 
        let peopleJsonData = await swapiMiddlewareResolver.searchPeopleData('Luke Skywalker');
        assert.strictEqual(deepCompare(staticPeople, peopleJsonData.results[0]), true);
    }).timeout(5000);
});