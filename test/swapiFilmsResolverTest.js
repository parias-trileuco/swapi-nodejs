const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticFilms} = require('./expectedResults');

describe('Swapi Films API', () => {
    it('It should return a JSON Object with getFilmsData data', async () => { 
        let filmJsonData = await swapiMiddlewareResolver.getFilmsData();
        assert.strictEqual(deepCompare(staticFilms, filmJsonData.results[0]), true);
    }).timeout(5000);
     
    it('It should return a unique JSON Object with getFilmsData data', async () => { 
        let filmJsonData = await swapiMiddlewareResolver.getFilmsData(1);
        assert.strictEqual(deepCompare(staticFilms, filmJsonData), true);
    }).timeout(5000);

    it('It should return a JSON Object with getFilmsData data', async () => {
        let filmJsonData = await swapiMiddlewareResolver.searchFilmsData('A New Hope');
        assert.strictEqual(deepCompare(staticFilms, filmJsonData.results[0]), true);
    }).timeout(5000);
});