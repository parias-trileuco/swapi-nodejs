const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticSpecies} = require('./expectedResults');

describe('Swapi Species API', () => {
  it('It should return a JSON Object with getSpeciesData data', async () => {
    let speciesJsonData = await swapiMiddlewareResolver.getSpeciesData();
    assert.strictEqual(deepCompare(staticSpecies, speciesJsonData.results[0]), true);
  }).timeout(5000);

  it('It should return a unique JSON Object with getSpeciesData data', async () => {
    let speciesJsonData = await swapiMiddlewareResolver.getSpeciesData(1);
    assert.strictEqual(deepCompare(staticSpecies, speciesJsonData), true);
  }).timeout(5000);

  it('It should return a JSON Object with searchSpeciesData data', async () => {   
    let speciesJsonData = await swapiMiddlewareResolver.searchSpeciesData('Human');
    assert.strictEqual(deepCompare(staticSpecies, speciesJsonData.results[0]), true);
  }).timeout(5000);
});