module.exports = {
    staticVehicles: staticVehicles = {
        name: 'Sand Crawler',
        model: 'Digger Crawler',
        manufacturer: 'Corellia Mining Corporation',
        cost_in_credits: '150000',
        length: '36.8 ',
        max_atmosphering_speed: '30',
        crew: '46',
        passengers: '30',
        cargo_capacity: '50000',
        consumables: '2 months',
        vehicle_class: 'wheeled',
        pilots: [],
        films: [
          'http://swapi.trileuco.com/api/films/1/',
          'http://swapi.trileuco.com/api/films/5/'
        ],
        created: '2014-12-10T15:36:25.724000Z',
        edited: '2014-12-20T21:30:21.661000Z',
        url: 'http://swapi.trileuco.com/api/vehicles/4/'
      },
      staticPeople: staticPeople = {
        name: 'Luke Skywalker',
        height: '172',
        mass: '77',
        hair_color: 'blond',
        skin_color: 'fair',
        eye_color: 'blue',
        birth_year: '19BBY',
        gender: 'male',
        homeworld: 'http://swapi.trileuco.com/api/planets/1/',
        films: [
          'http://swapi.trileuco.com/api/films/1/',
          'http://swapi.trileuco.com/api/films/2/',
          'http://swapi.trileuco.com/api/films/3/',
          'http://swapi.trileuco.com/api/films/6/'
        ],
        species: [],
        vehicles: [
          'http://swapi.trileuco.com/api/vehicles/14/',
          'http://swapi.trileuco.com/api/vehicles/30/'
        ],
        starships: [
          'http://swapi.trileuco.com/api/starships/12/',
          'http://swapi.trileuco.com/api/starships/22/'
        ],
        created: '2014-12-09T13:50:51.644000Z',
        edited: '2014-12-20T21:17:56.891000Z',
        url: 'http://swapi.trileuco.com/api/people/1/'
      },
      staticFilms: staticFilms = {
        title: 'A New Hope',
        episode_id: 4,
        opening_crawl: 'It is a period of civil war.\r\n' +
          'Rebel spaceships, striking\r\n' +
          'from a hidden base, have won\r\n' +
          'their first victory against\r\n' +
          'the evil Galactic Empire.\r\n' +
          '\r\n' +
          'During the battle, Rebel\r\n' +
          'spies managed to steal secret\r\n' +
          "plans to the Empire's\r\n" +
          'ultimate weapon, the DEATH\r\n' +
          'STAR, an armored space\r\n' +
          'station with enough power\r\n' +
          'to destroy an entire planet.\r\n' +
          '\r\n' +
          "Pursued by the Empire's\r\n" +
          'sinister agents, Princess\r\n' +
          'Leia races home aboard her\r\n' +
          'starship, custodian of the\r\n' +
          'stolen plans that can save her\r\n' +
          'people and restore\r\n' +
          'freedom to the galaxy....',
        director: 'George Lucas',
        producer: 'Gary Kurtz, Rick McCallum',
        release_date: '1977-05-25',
        characters: [
          'http://swapi.trileuco.com/api/people/1/',
          'http://swapi.trileuco.com/api/people/2/',
          'http://swapi.trileuco.com/api/people/3/',
          'http://swapi.trileuco.com/api/people/4/',
          'http://swapi.trileuco.com/api/people/5/',
          'http://swapi.trileuco.com/api/people/6/',
          'http://swapi.trileuco.com/api/people/7/',
          'http://swapi.trileuco.com/api/people/8/',
          'http://swapi.trileuco.com/api/people/9/',
          'http://swapi.trileuco.com/api/people/10/',
          'http://swapi.trileuco.com/api/people/12/',
          'http://swapi.trileuco.com/api/people/13/',
          'http://swapi.trileuco.com/api/people/14/',
          'http://swapi.trileuco.com/api/people/15/',
          'http://swapi.trileuco.com/api/people/16/',
          'http://swapi.trileuco.com/api/people/18/',
          'http://swapi.trileuco.com/api/people/19/',
          'http://swapi.trileuco.com/api/people/81/'
        ],
        planets: [
          'http://swapi.trileuco.com/api/planets/1/',
          'http://swapi.trileuco.com/api/planets/2/',
          'http://swapi.trileuco.com/api/planets/3/'
        ],
        starships: [
          'http://swapi.trileuco.com/api/starships/2/',
          'http://swapi.trileuco.com/api/starships/3/',
          'http://swapi.trileuco.com/api/starships/5/',
          'http://swapi.trileuco.com/api/starships/9/',
          'http://swapi.trileuco.com/api/starships/10/',
          'http://swapi.trileuco.com/api/starships/11/',
          'http://swapi.trileuco.com/api/starships/12/',
          'http://swapi.trileuco.com/api/starships/13/'
        ],
        vehicles: [
          'http://swapi.trileuco.com/api/vehicles/4/',
          'http://swapi.trileuco.com/api/vehicles/6/',
          'http://swapi.trileuco.com/api/vehicles/7/',
          'http://swapi.trileuco.com/api/vehicles/8/'
        ],
        species: [
          'http://swapi.trileuco.com/api/species/1/',
          'http://swapi.trileuco.com/api/species/2/',
          'http://swapi.trileuco.com/api/species/3/',
          'http://swapi.trileuco.com/api/species/4/',
          'http://swapi.trileuco.com/api/species/5/'
        ],
        created: '2014-12-10T14:23:31.880000Z',
        edited: '2014-12-20T19:49:45.256000Z',
        url: 'http://swapi.trileuco.com/api/films/1/'
      },
      staticStarship: staticStarship = {
        name: 'CR90 corvette',
        model: 'CR90 corvette',
        manufacturer: 'Corellian Engineering Corporation',
        cost_in_credits: '3500000',
        length: '150',
        max_atmosphering_speed: '950',
        crew: '30-165',
        passengers: '600',
        cargo_capacity: '3000000',
        consumables: '1 year',
        hyperdrive_rating: '2.0',
        MGLT: '60',
        starship_class: 'corvette',
        pilots: [],
        films: [
          'http://swapi.trileuco.com/api/films/1/',
          'http://swapi.trileuco.com/api/films/3/',
          'http://swapi.trileuco.com/api/films/6/'
        ],
        created: '2014-12-10T14:20:33.369000Z',
        edited: '2014-12-20T21:23:49.867000Z',
        url: 'http://swapi.trileuco.com/api/starships/2/'
      },

      staticSpecies: staticSpecies = {
        name: 'Human',
        classification: 'mammal',
        designation: 'sentient',
        average_height: '180',
        skin_colors: 'caucasian, black, asian, hispanic',
        hair_colors: 'blonde, brown, black, red',
        eye_colors: 'brown, blue, green, hazel, grey, amber',
        average_lifespan: '120',
        homeworld: 'http://swapi.trileuco.com/api/planets/9/',
        language: 'Galactic Basic',
        people: [
          'http://swapi.trileuco.com/api/people/66/',
          'http://swapi.trileuco.com/api/people/67/',
          'http://swapi.trileuco.com/api/people/68/',
          'http://swapi.trileuco.com/api/people/74/'
        ],
        films: [
          'http://swapi.trileuco.com/api/films/1/',
          'http://swapi.trileuco.com/api/films/2/',
          'http://swapi.trileuco.com/api/films/3/',
          'http://swapi.trileuco.com/api/films/4/',
          'http://swapi.trileuco.com/api/films/5/',
          'http://swapi.trileuco.com/api/films/6/'
        ],
        created: '2014-12-10T13:52:11.567000Z',
        edited: '2014-12-20T21:36:42.136000Z',
        url: 'http://swapi.trileuco.com/api/species/1/'
      },

      staticPlanets: staticPlanets = {
        name: 'Tatooine',
        rotation_period: '23',
        orbital_period: '304',
        diameter: '10465',
        climate: 'arid',
        gravity: '1 standard',
        terrain: 'desert',
        surface_water: '1',
        population: '200000',
        residents: [
          'http://swapi.trileuco.com/api/people/1/',
          'http://swapi.trileuco.com/api/people/2/',
          'http://swapi.trileuco.com/api/people/4/',
          'http://swapi.trileuco.com/api/people/6/',
          'http://swapi.trileuco.com/api/people/7/',
          'http://swapi.trileuco.com/api/people/8/',
          'http://swapi.trileuco.com/api/people/9/',
          'http://swapi.trileuco.com/api/people/11/',
          'http://swapi.trileuco.com/api/people/43/',
          'http://swapi.trileuco.com/api/people/62/'
        ],
        films: [
          'http://swapi.trileuco.com/api/films/1/',
          'http://swapi.trileuco.com/api/films/3/',
          'http://swapi.trileuco.com/api/films/4/',
          'http://swapi.trileuco.com/api/films/5/',
          'http://swapi.trileuco.com/api/films/6/'
        ],
        created: '2014-12-09T13:50:49.641000Z',
        edited: '2014-12-20T20:58:18.411000Z',
        url: 'http://swapi.trileuco.com/api/planets/1/'
      },
      staticPersonInfo: staticPersonInfo = {
        name: 'Luke Skywalker',
        gender: 'male',
        planet_name: 'Tatooine',
        films: [
          { title: 'A New Hope', release_date: '1977-05-25' },
          { title: 'The Empire Strikes Back', release_date: '1980-05-17' },
          { title: 'Return of the Jedi', release_date: '1983-05-25' },
          { title: 'Revenge of the Sith', release_date: '2005-05-19' }
        ],
        fastest_vehicle_driven: 'X-wing'
      },
      
      staticPersonInfoPage: staticPersonInfoPage = {
        "count":82,"next":"http://swapi.trileuco.com/api/people/?page=3",
        "previous":"http://swapi.trileuco.com/api/people/?page=1",
        "results":
        [
            {"name":"Anakin Skywalker","gender":"male","planet_name":"Tatooine","films":[{"title":"The Phantom Menace","release_date":"1999-05-19"},{"title":"Attack of the Clones","release_date":"2002-05-16"},{"title":"Revenge of the Sith","release_date":"2005-05-19"}],"fastest_vehicle_driven":"Jedi Interceptor"},
            {"name":"Wilhuff Tarkin","gender":"male","planet_name":"Eriadu","films":[{"title":"A New Hope","release_date":"1977-05-25"},{"title":"Revenge of the Sith","release_date":"2005-05-19"}],"fastest_vehicle_driven":"n/a"},
            {"name":"Chewbacca","gender":"male","planet_name":"Kashyyyk","films":[{"title":"A New Hope","release_date":"1977-05-25"},{"title":"The Empire Strikes Back","release_date":"1980-05-17"},{"title":"Return of the Jedi","release_date":"1983-05-25"},{"title":"Revenge of the Sith","release_date":"2005-05-19"}],"fastest_vehicle_driven":"Millennium Falcon"},
            {"name":"Han Solo","gender":"male","planet_name":"Corellia","films":[{"title":"A New Hope","release_date":"1977-05-25"},{"title":"The Empire Strikes Back","release_date":"1980-05-17"},{"title":"Return of the Jedi","release_date":"1983-05-25"}],"fastest_vehicle_driven":"n/a"},{"name":"Greedo","gender":"male","planet_name":"Rodia","films":[{"title":"A New Hope","release_date":"1977-05-25"}],"fastest_vehicle_driven":"n/a"},
            {"name":"Jabba Desilijic Tiure","gender":"hermaphrodite","planet_name":"Nal Hutta","films":[{"title":"A New Hope","release_date":"1977-05-25"},{"title":"Return of the Jedi","release_date":"1983-05-25"},{"title":"The Phantom Menace","release_date":"1999-05-19"}],"fastest_vehicle_driven":"n/a"},
            {"name":"Wedge Antilles","gender":"male","planet_name":"Corellia","films":[{"title":"A New Hope","release_date":"1977-05-25"},{"title":"The Empire Strikes Back","release_date":"1980-05-17"},{"title":"Return of the Jedi","release_date":"1983-05-25"}],"fastest_vehicle_driven":"X-wing"},
            {"name":"Jek Tono Porkins","gender":"male","planet_name":"Bestine IV","films":[{"title":"A New Hope","release_date":"1977-05-25"}],"fastest_vehicle_driven":"n/a"},{"name":"Yoda","gender":"male","planet_name":"unknown","films":[{"title":"The Empire Strikes Back","release_date":"1980-05-17"},{"title":"Return of the Jedi","release_date":"1983-05-25"},{"title":"The Phantom Menace","release_date":"1999-05-19"},{"title":"Attack of the Clones","release_date":"2002-05-16"},{"title":"Revenge of the Sith","release_date":"2005-05-19"}],"fastest_vehicle_driven":"n/a"},
            {"name":"Palpatine","gender":"male","planet_name":"Naboo","films":[{"title":"The Empire Strikes Back","release_date":"1980-05-17"},{"title":"Return of the Jedi","release_date":"1983-05-25"},{"title":"The Phantom Menace","release_date":"1999-05-19"},{"title":"Attack of the Clones","release_date":"2002-05-16"},{"title":"Revenge of the Sith","release_date":"2005-05-19"}],"fastest_vehicle_driven":"n/a"}
        ]
    }
};
