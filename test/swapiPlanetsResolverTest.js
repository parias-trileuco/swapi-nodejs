const swapiMiddlewareResolver = require('../server/swapiMiddlewareResolver.js');
const assert = require('assert');
const {deepCompare} = require('./utils');
const {staticPlanets} = require('./expectedResults');

describe('Swapi Planets API', () => {
  it('It should return a JSON Object with getPlanetsData data', async () => {   
    let planetJsonData = await swapiMiddlewareResolver.getPlanetsData();
    assert.strictEqual(deepCompare(staticPlanets, planetJsonData.results[0]), true);
  }).timeout(5000);
    
  it('It should return a unique JSON Object with getPlanetsData data', async () => {  
    let planetJsonData = await swapiMiddlewareResolver.getPlanetsData(1);
    assert.strictEqual(deepCompare(staticPlanets, planetJsonData), true);
  }).timeout(5000);

  it('It should return a JSON Object with searchPlanetsData data', async () => {  
    let planetJsonData = await swapiMiddlewareResolver.searchPlanetsData('Tatooine');
    assert.strictEqual(deepCompare(staticPlanets, planetJsonData.results[0]), true);
  }).timeout(5000);
});
