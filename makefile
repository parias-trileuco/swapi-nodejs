INSTALL = ./bin/install.sh --yes
UNINSTALL = ./bin/uninstall.sh --yes
TRAVIS = ./bin/install-travis.sh
RUN = npm run dev
TEST_BACKEND = npm test

run-tests:
	$(TEST_BACKEND)

install:
	$(INSTALL)

uninstall:
	$(UNINSTALL)

run:
	$(RUN)

reinstall:
	@make uninstall
	@make install

