# source: https://github.com/vercel/install-node/blob/master/install.sh

YELLOW="$(tput setaf 3 2>/dev/null || echo '')"
BLUE="$(tput setaf 4 2>/dev/null || echo '')"
MAGENTA="$(tput setaf 5 2>/dev/null || echo '')"
CYAN="$(tput setaf 6 2>/dev/null || echo '')"
NO_COLOR="$(tput sgr0 2>/dev/null || echo '')"

while [ "$#" -gt 0 ]; do
  case "$1" in
    --yes) FORCE=1; shift 1;;
    -*) error "Unknown option: $1"; exit 1;;
  esac
done

info() {
  printf "${BOLD}${GREY}>${NO_COLOR} $@\n"
}

warn() {
  printf "${YELLOW}! $@${NO_COLOR}\n"
}

error() {
  printf "${RED}x $@${NO_COLOR}\n" >&2
}

complete() {
  printf "${GREEN}✓${NO_COLOR} $@\n"
}

confirm() {
  if [ -z "${FORCE-}" ]; then
    printf "${MAGENTA}?${NO_COLOR} $@ ${BOLD}[y/n]${NO_COLOR} "
    set +e
    read yn < /dev/tty
    rc=$?
    set -e
    if [ $rc -ne 0 ]; then
      error "Error reading from prompt (please re-run with the \`--yes\` option)"
      exit 1
    fi
    if [ "$yn" != "y" ] && [ "$yn" != "yes" ]; then
      error "Aborting (please answer \"yes\" to continue)"
      exit 1
    fi
  fi
}

confirm "Uninstall dependencies for swapi-nodejs ${GREEN}${NO_COLOR}?"
info "removing swapi-nodejs server dependencies ..."
rm -rf ./node_modules/
rm package-lock.json
complete "swapi-nodejs server dependencies removed!"
info "removing swapi-nodejs client dependencies ..."
rm -rf ./client/node_modules/
complete "swapi-nodejs server dependencies removed!"


# Purge All global packages from npm
# sudo npm list -g --depth=0. | awk -F ' ' '{print $2}' | awk -F '@' '{print $1}'  | sudo xargs npm remove -g
# sudo npm ls -gp --depth=0 | awk -F/ '/node_modules/ && !/\/npm$/ {print $NF}' | xargs npm -g rm
# sudo npm list -g --depth 0