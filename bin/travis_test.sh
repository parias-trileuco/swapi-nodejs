#!/bin/bash

set -e

npm run dev 2> /dev/null 1> /dev/null &
sleep 3 # to make sure the swapi-nodejs is running
npm test

kill -9 $(lsof -t -i:3000)
kill -9 $(lsof -t -i:8080)