## Manual Installation:

``` bash
# Install dependencies for server
npm install

# Install dependencies for client
npm run client-install
```

## Automatic Installation:

``` bash
# Install dependencies for server
chmod +x ./swapi-node-installer.sh
./swapi-node-installer.sh
```

## Quick Start:

``` bash
# Run the client & server with concurrently
npm run dev

# Run the Express server only
npm run server

# Run the React client only
npm run client

# Server runs on http://localhost:8080 and client on http://localhost:3000
```

Install Node 

sudo apt update
sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -