import React, { Component } from 'react';
import './swapiPersonInfo.css';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import ListGroupItem from 'react-bootstrap/ListGroupItem';


class SwapiPersonInfo extends Component {
    render () {
        const isLoadingIn = this.props.isLoadingIn;
        var name = this.props.name;
        var gender = this.props.gender;
        var planet_name = this.props.planet_name;
        var fastest_vehicle_driven = this.props.fastest_vehicle_driven

        if (this.props.name === undefined) {
            name = 'N/A'
            gender = 'N/A'
            planet_name = 'N/A'
            fastest_vehicle_driven = 'N/A'
        }
        if (isLoadingIn) {
            return null;
        } else {
            return (
                <div>
                     <Card style={{ width: '20rem' }}>
                     <Card.Header as="h4">{name}</Card.Header>
                     <Card.Body>
                         <Card.Subtitle className="mb-1 text-muted">Gender: {gender}</Card.Subtitle>
                         <Card.Subtitle className="mb-1 text-muted">Home Planet: {planet_name}</Card.Subtitle>
                         <Card.Subtitle className="mb-1 text-muted">Fastest Vehicle: {fastest_vehicle_driven}</Card.Subtitle>
                     </Card.Body>
                     <Card.Header as="h6" style={{ higth: '10rem' }}></Card.Header>
                     <ListGroup className="list-group-flush">
                         {this.props.films && this.props.films.map(film => <ListGroupItem className="swapi-black-text-italic" key={film.title}> {film.title}, {film.release_date}</ListGroupItem>)}
                     </ListGroup>
                     </Card>
                 </div>
             )
        }
    }
}

export default SwapiPersonInfo;