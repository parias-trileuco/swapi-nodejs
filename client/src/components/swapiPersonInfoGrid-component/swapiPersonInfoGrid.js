import React, { Component } from 'react';
import SwapiLoader from '../swapiLoader-component/swapiLoader';
import SwapiPersonInfo from '../swapiPersonInfo-component/swapiPersonInfo';
import CardDeck from 'react-bootstrap/CardDeck'
import SwapiPagination from '../swapiPagination-component/swapiPagination';
import SwapiNavBarSearch from '../swapiNavBar-component/swapiNavBarSearch';

// https://itnext.io/back-end-pagination-with-nodejs-expressjs-mongodb-mongoose-ejs-3566994356e0
// https://stackoverflow.com/questions/23123138/perform-debounce-in-react-js
// https://stackoverflow.com/questions/14226803/wait-5-seconds-before-executing-next-line/47480429
// https://github.com/slorber/awesome-debounce-promise

class SwapiPersonInfoGrid extends Component {    
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.state = {
            personList: [], // attr for storing fetched data from swapi api
            isLoadingIn: true,  // attr for controlling the flow of loading while displaying gridInfoPerson contents
            currentPage: 1, // attr for the default page number
            pageSize: 10,   // attr for the default page size
            searchItem: '',
            searchResult: {"results": []},
            pageCount: 0,
 
          };
    }
    
    componentDidMount() {
      this.loadPage();
    }

    componentWillUnmount() {
      this.handleClearSearch();
    }

    componentDidUpdate(prevProps, prevState) {
      // Pagination Click
      if(prevState.currentPage !== this.state.currentPage) {
        this.handleOnLoading();
        this.loadPage();

       // Search has been cleared, searchResult: null, to recover the view of personInfo, from previous request.
      } else if (prevState.searchItem !== '' && this.state.searchItem === '') {
          this.handleClearSearch();
      }  
    }

    /**
     * @abstract handleOnLoading: This function, sets the isLoadingIn as false
     */
    handleOffLoading() {
      this.setState({ isLoadingIn: false });
    }

    /**
     * @abstract handleOnLoading: This function, sets the isLoadingIn as true 
     */
    handleOnLoading() {
      this.setState({ isLoadingIn: true });
    }

    /**
     * @abstract handleClearSearch: This function, handles clear of the input data from search. 
     */
    handleClearSearch() {
      this.setState({ searchResult: {"results": []}});
      this.setState({ searchItem: ''});
    }

    /**
     * @abstract handleSearch: This function, handles the search query input from the navbar component. 
     * @notice Atleast 2 characters must be written on the input to trigger action)
     * @param {Event} event 
     */
    async handleSearch (event) {   
      this.handleClearSearch()
      if( event.target.value.trim().length >= 2) {
        this.loadSearch(event.target.value.trim().toLowerCase());
        this.handleOnLoading();
      } 
    }

    /**
     * @abstract handleClick: handle click on pagination component
     * @param {Event} event 
     */
    handleClick(event) {
        event.preventDefault();
        this.setState({ currentPage: Number(event.target.id) });      
    }

    /**
     * @abstract loadSearch: This function, handles the fetching from the backend regarding info-person functionalities
     */
    loadPage() {      
      const { pageSize } = this.state;
      fetch(`/swapi-proxy/person-info?page=${this.state.currentPage}`)
      .then(res => res.json())
      .then(resultJsonData => this.setState( {personList: resultJsonData}, 
        () => console.log(` [ SwapiWeb ]:  resultJsonData from loadPage:`, resultJsonData)))
      .then(() => this.setState( { pageCount: Math.ceil(this.state.personList.count / pageSize) },
      () => console.log(` [ SwapiWeb ]:  pageCount: ${this.state.pageCount}`)))
      .then(() => this.handleOffLoading())
      .catch(error => console.log(` [ SwapiWeb ]:  Unhandled Exception:\n${error}`));
      console.log(` [ SwapiWeb ]:  /swapi-proxy/person-info?page=${this.state.currentPage} fetched ...`);
    }

    /**
     * @abstract loadSearch: This function, handles the fetching from the backend regarding search functionalities
     */
    loadSearch(text) {
      fetch(`/swapi-proxy/person-info?name=${text}`)
      .then(res => { if (res.status === 200) { return res.json(); } 
        else if ( res.status === 404) { return []; }})
      .then(resultJsonData => this.setState( {searchResult: {"results": [resultJsonData]}},  
        () => console.log(` [ SwapiWeb ]:  resultJsonData from searchPage:`, resultJsonData)))
      .then(() => this.handleOffLoading())
      .catch(error => console.log(` [ SwapiWeb ]:  Unhandled Exception:\n${error}`));
      console.log(` [ SwapiWeb ]: /swapi-proxy/person-info?name=${this.state.searchItem} fetched ...`);
    }

    render () {
      const { isLoadingIn, personList, searchResult, pageCount } = this.state;
      let setOfCards;

      console.log(searchResult, searchResult.results[0] !== undefined )
     
      if(searchResult.results[0] !== undefined) {
        setOfCards = searchResult;
      } else {
        setOfCards = personList;
      }

      return (
        <div className="App">
        <SwapiNavBarSearch isLoadingIn={isLoadingIn} handleSearch={this.handleSearch}/>
        <SwapiLoader isLoadingIn={isLoadingIn}/>
          <div className="card-container">
            <CardDeck>
            {setOfCards.results && setOfCards.results.map((personInfo, index) => {
              return (
                <SwapiPersonInfo isLoadingIn={isLoadingIn} key={'Card-' + index + '-PersonInfo'}
                 name={personInfo.name} gender={personInfo.gender} planet_name={personInfo.planet_name}
                 fastest_vehicle_driven={personInfo.fastest_vehicle_driven} films={personInfo.films} />
              )})}
            </CardDeck>
            <SwapiPagination isLoadingIn={isLoadingIn} handleClick={this.handleClick} pageCount={pageCount} />
          </div>                    
        </div> 
      )
    }     
}
export default SwapiPersonInfoGrid;