import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import FormControl from 'react-bootstrap/FormControl';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';

class SwapiNavBar extends Component {
    render() {
        let searchInput;
        const isLoadingIn = this.props.isLoadingIn;
        if (isLoadingIn) {
            searchInput = (
                <Form disabled inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-3"  onChange={this.props.handleSearch} />
                </Form>
            ) 
        } else {
            searchInput = (
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-3"  onChange={this.props.handleSearch} />
                </Form>
            ) 
        }
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="/">[ Swapi-Proxy ]</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/about">About</Nav.Link>
                </Nav>
                {searchInput}
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default SwapiNavBar;