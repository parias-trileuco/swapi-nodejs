import React, { Component } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import '../swapiPersonInfo-component/swapiPersonInfo.css';

// https://stackoverflow.com/questions/47800245/node-pagination-with-express

class SwapiPagination extends Component {
    render () {
        const _array = new Array(this.props.pageCount);
        const arrayDummy = _array.fill('');
        const isLoadingIn = this.props.isLoadingIn;

        if(isLoadingIn) {
          return null
        } else {
          return (
            <div className="pagination-container">
              <Pagination>
              { arrayDummy.map((data, index) => {
                  return (
                      <Pagination.Item key={index+data} id={index+1} onClick={this.props.handleClick}>{index+1}</Pagination.Item>
                  )
              })}
              </Pagination>
            </div>
          
          );
        }
    }
}

export default SwapiPagination;