import React, { Component } from 'react';


import SwapiPersonInfoGrid from '../components/swapiPersonInfoGrid-component/swapiPersonInfoGrid';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

class Home extends Component {
  render() {
    return (
    <div className="App">
      <SwapiPersonInfoGrid/>
   </div>
    );
  }
}
export default Home;

