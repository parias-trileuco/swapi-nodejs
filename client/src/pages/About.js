import React, { Component } from 'react';
import SwapiNavBar from '../components/swapiNavBar-component/swapiNavBar';
import Card from 'react-bootstrap/Card';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class About extends Component {
  render() {
    return (
      <div className="container-about">
        <SwapiNavBar/> 
        <br/>
        <Card style={{ width: '25rem' }}>
          <Card.Body>
            <Card.Title>Landing Weeks: Trileuco Solutions</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Pedro Arias Ruiz, Software Engineer</Card.Subtitle>
            <Card.Text>
              Develop a swapi-proxy using node-js as the main framework in conjunction 
              with react-js for the front-end and express for the back-end.
            </Card.Text>
            <Card.Link href="http://swapi.trileuco.com/">Swapi API</Card.Link>
          </Card.Body>
        </Card>
        <br/>
      </div>

    );
  }
}
export default About;
