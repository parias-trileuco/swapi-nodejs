import React from 'react';
import PageNotFound from '../assets/img/PageNotFound.png';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

class NotFoundPage extends React.Component{
    render(){
        return (      
            <div className="App">
                <br/>
                <img src={PageNotFound} style={{textAlign:"center"}} alt="pageNotFoundImage"/>  
                <p style={{textAlign:"center"}}>
                  <Link to="/">Go to Home </Link>
                </p>
                <br/>
          </div>
        );
    }
}export default NotFoundPage;